#!/usr/bin/python3

from pathlib import Path
from html.parser import HTMLParser

text = []
# input_file = input('HTML file:') or 'index.html'
         

def parse_data(data):
    last_c = len(data) - 1
    string = data
    if data[0] == ' ':
        string = string[1:]
    if data[last_c] == ' ':
        string = string[0:last_c]
    return string


def parse_text(text):
    return ''.join(text.split('\n'))


class MyHTMLParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        output_file = open('output.txt', 'a')
        if tag == 'div':
            if attrs:
                if attrs[0][1] == 'cadre_menu':
                    return output_file.write('\n{menu}\n')
            return output_file.write('\n{div}\n')
        output_file.write(f'<{tag}> ')
        output_file.close()
        '''
        if tag == 'div':
            return text.append('\n{div}\n')
        text.append(f'<{tag}> ')
        '''

    def handle_endtag(self, tag):
        output_file = open('output.txt', 'a')
        if tag == 'form' or tag == 'div':
            return output_file.write(f'\n</{tag}>\n')
        output_file.write(f' </{tag}>\n')
        output_file.close()
        '''
        if tag == 'form':
            return text.append('\n{form}\n')
        text.append(f' </{tag}>\n')
        '''

    def handle_data(self, data):
        if not data:
            return
        output_file = open('output.txt', 'a')
        output_file.write(data)
        output_file.close()
        '''
        text.append(data)
        '''

def html_to_text(html):
    output_file = open('output.txt', 'w')
    output_file.close()
    parser = MyHTMLParser()
    content = parse_text(html)
    parser.feed(content)
    result = open('output.txt', 'r').read()
    return result


def to_csv(html):
    sep = ';'
    new_html = html_to_text(html).split('\n')
    i = new_html.index('</form>')
    html = []
    for line in new_html[i::]:
        new_line = [ x for x in line.split(' ') if x ]
        for word in new_line:
            #print(word)
            if not word[0] == '<':
                html.append(word + ' ')
        html.append('\n') 
    html = ''.join(html)
    html = html.split('\n')    
    #csv = [f'SEP={sep}']
    csv = []
    for i, word in enumerate(html):
        if '::' in word:
            french_word = word[3:len(word) - 1]
            creole_word = html[i-1]
            #print(french_word, end=', ')
            #print(creole_word)
            csv.append(f'{french_word}{sep}{creole_word}')
    return csv        

'''
html = open('pages/a_2.html').read()
csv = to_csv(html)
print(csv)
'''
