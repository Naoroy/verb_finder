#!/usr/bin/python3


import requests
from pathlib import Path
from html_parser import to_csv
from html_parser import html_to_text

# import parser class
# import verb finder
# find verbs, output csv file
# create an array to keep filenames
# or a dict to keep html instead of files


def main():
    filenames = download_html()
    #filenames = get_files()
    verb_csv = []
    for filename in filenames:
        current_file = open(f'pages/{filename}')
        html = current_file.read()
        current_file.close()
        csv = to_csv(html)

        clean_csv = []
        for line in csv:
            line = ''.join(line.split('.'))
            #line = ''.join(line.split(','))
            clean_csv.append(line)

        lines = [ line for line in clean_csv if found_verb_in(line) ]
        verb_csv.append('\n'.join(lines))

    sep = ';'
    verb_csv.insert(0, f'SEP={sep}')
    output_csv = open('out.csv', 'w')
    output_csv.write('\n'.join(verb_csv))
    output_csv.close()


# find filenames of existing html files and append to list
def get_files():
    filenames = []
    p = Path('pages')
    paths = sorted(p.glob('*.html'))
    for f in paths:
        filenames.append(f.name)
    return filenames


def page_count(letter):
    page_count = 1
    response = get_url(letter, 1)
    html = html_to_text(response.text).split('\n')

    start = html.index('{menu}')
    html = html[start + 1::]

    start = html.index('{menu}')
    html = html[start + 1::]
    end = html.index('</div>')
    return end - 1


# Create html files and return filename List
def download_html():
    filenames = []
    for ascii_letter in range(97, 123): 
        char = chr(ascii_letter)
        for page in range(1, page_count(char) + 1):
            response = get_url(char, page)
            if response:
                filenames.append(make_file(char, page, response.text))
                print(f'char: {char}, page: {page}')
            else:
                print('Something went wrong')
    return filenames
    

def make_file(char, page, html):
    filename = f'{char}_{page}.html'
    html_file = open(f'pages/{filename}', 'w')
    html_file.write(html)
    html_file.close()
    return filename


def get_url(char, page):
    url = 'http://www.mi-aime-a-ou.com/dictionnaire_creole_lettre.php'
    param = f'?creole={char}&page={page}'
    response = requests.get(url + param)
    if response.status_code == 200:
        return response


def found_verb_in(line):
    suffixes = [ 
        'er',
        'ir',
        #'oir',
        're'
    ]

    french_words = line.split(';')[0].split(' ')
    
    for word in french_words:
        if word[-2::] in suffixes:
            return True
    return False
    '''
    print(line)
    for suffix in suffixes:
        if suffix in line:
            return True
    return False
    '''


if __name__ == '__main__':
    main()
    #print(page_count('z'))
