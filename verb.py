#!/usr/bin/python3


# iterate through file and put each line in a list
# filter the list, only keep /b tag and the line before the tag


def make_list(filename):
    lines = []
    # search for one of the suffixes in the line
    with open(filename) as text:
        for line in text:
            lines.append(line) 
        return lines


def found_verb_in(line):
    suffixes = [ 
        'er',
        'ir',
        'oir',
        're'
    ]
    #print(line)
    for suffix in suffixes:
        if suffix in line:
            return True
    return False


def parse_line(line):
    new_line = ''.join(line.split('.')) if '.' in line else line
    new_line = ''.join(line.split(',')) if ',' in line else new_line
    o = new_line.find('>')
    new_line = new_line[o+2:]
    o = new_line.find('<')
    return new_line[:o]
    

def has_b_tag(line):
    for i, char in enumerate(line):
        if char == 'b':
            string = line[i - 1] + char + line[i + 1]
            if string == '<b>':
                print('yes')
                return True


def do_it(filename):
    verb_file = open('verbs.csv', 'a')
    lines = make_list(f'pages/{filename}')
    for i, line in enumerate(lines):
        if has_b_tag(line): 
            prev_line = parse_line(lines[i - 1])
            line = parse_line(line)
            if not found_verb(line):
                pass
            else:
                formated_line = f'{prev_line},{line}\n'
                verb_file.write(formated_line)
                print(formated_line)
    verb_file.close()
